# <center>Gestion de fichiers</center>

Par David Captaine et Luc Bournonville

## 1 : Activité préliminaire sur *Windows* avec l'explorateur de fichiers

**Mise en situation:**  
Alice est très organisée. Chaque fichier qu'elle crée avec un logiciel ou tous les fichiers qu'elle reçoit ou télécharge sont rangés dans des dossiers spécifiques. Ceci l'aide à retrouver plus facilement chacun des fichiers.  
Mais hier, Alice a quitté la salle informatique en oubliant de fermer sa session.  
Quelques instants plus tard, Bob s'installe à la place d'Alice et constate que la session est restée ouverte. Bob connait bien Alice et décide de lui "faire une blague" en mettant tous les fichiers dans le dossier "mes documents" et en supprimant tous les dossiers devenus vides.  
Quand Alice se connecte à nouveau, elle constate l'étendue des dégâts. Alice va devoir reconstruire "son arborescence"  et replacer chaque fichier au bon "endroit" c'est à dire dans le bon dossier.  
Alice se souvient de son arborescence, elle la dessine et elle obtient ceci:

![arborescence d'Alice](arbre_nsi.png)

### 1.1 - Travail sur les fichiers

Ci-dessous sont listés quelques fichiers qu'Alice devra ranger dans des dossiers.  
Chaque fichier est caractérisé par son nom (exemple) et son extension (.odt)  . Cette extension peut fournir des renseignements sur le type du fichier et les logiciels capables d'ouvrir ou de traiter ces fichiers.
Pour simplifier, nous considérerons que les fichiers sont de 3 catégories:

- documents (classeurs, textes,  etc....)
- multimédia (son, vidéo, image)
- divers (python, scratch, windows etc...)

Complétez le tableau suivant, en indiquant pour chaque fichier dans quelle catégorie il doit être rangé et un logiciel (gratuit si possible) capable de lire ce fichier. Vous pourrez faire des recherches sur internet pour vous aider si besoin.

Tableau à compléter pour quelques fichiers d'Alice

<table>
<tbody>
<thead style=' border:1px solid black'>
<th>fichier</th>
<th style="width:150px;">type de fichier</th>
<th style="width:200px;">logiciel</th>
</thead>
<tr>
<td>exemple.odt</td>
<td style="width:150px;">document</td>
<td style="width:200px;">LibreOffice</td>
</tr>
<tr>
<td>fic1.pdf</td>
<td></td>
<td></td>
</tr>
<tr>
<td>fic2.ggb</td>
<td></td>
<td></td>
</tr>
 <tr>
<td>fic2.jpg</td>
<td></td>
<td></td>
</tr>
    <tr>
<td>fic3.mp3</td>
<td></td>
<td></td>
</tr>
   <tr>
<td>fic4.docx</td>
<td></td>
<td></td>
</tr>
   <tr >
<td>fic5.mp4</td>
<td></td>
<td></td>
</tr>
   <tr>
<td>fic6.py</td>
<td></td>
<td></td>
</tr>
   <tr>
<td>fic7.txt</td>
<td></td>
<td></td>
</tr>
   <tr>
<td>fic8.xlsx</td>
<td></td>
<td></td>
</tr>
   <tr>
<td>fic9.csv</td>
<td></td>
<td></td>
</tr>
   <tr>
<td>fic10.png</td>
<td></td>
<td></td>
</tr>
   <tr>
<td>fic11.ods</td>
<td></td>
<td></td>
</tr>
   <tr>
<td>fic12.gif</td>
<td></td>
<td></td>
</tr>
   <tr>
<td>fic13.exe</td>
<td></td>
<td></td>
</tr
 <tr>
<td>fic14.zip</td>
<td></td>
<td></td>
</tr
</tbody>
</table>

### 1.2 - Création de l'arborescence

> Ouvrez le gestionnaire de fichiers.

> Placez-vous sur le lecteur nom.prenom  .

> Créez un dossier *Alice*.

> Dans Alice créez deux sous-dossiers *Personnel* et *Travail*.

> Continuez ainsi jusqu'à reproduire le schéma proposé ci-dessus.

### 1.3 - Rangement des fichiers

> Dans le lecteur *public M:*, dans le dossier *gr_snt*, copiez le dossier *fichiers_Alice.zip* dans votre dossier *Alice*.

> Décompresser ce fichier.

> Supprimer le fichier *fichiers_Alice.zip*

> Recopier dans le tableau ci-dessous, les noms des fichiers et le complétez avec le nom du dossier dans lequel vous pensez qu'il pourrait ou devrait être rangé.

<table>
<tr style=' border:1px solid black'>
<!-- en-tête commune à deux colonnes-->    
<th colspan="3"><center>Proposition de placement des fichiers</center></th>     
</tr>
<tr style=' border:1px solid black'> 
<th>Numéro</th> 
<th>Nom du fichier</th>
<th style="width:300px;">Dossier de destination</th>
</tr>
<tr style=' border:1px solid black' >
<td>0</td>
<td>exemple.odt</td>
<td>maths/cours</td>
</tr>
<tr style=' border:1px solid black' >
<td>1</td>
<td></td>
<td></td>
</tr> 
<tr style=' border:1px solid black'>
<td>2</td>
<td></td>
<td></td>
</tr> 
<tr style=' border:1px solid black'>
<td>3</td>
<td></td>
<td></td>
</tr> 
<tr style=' border:1px solid black'>
<td>4</td>
<td></td>
<td></td>
</tr> 
<tr style=' border:1px solid black'>
<td>5</td>
<td></td>
<td></td>
</tr>
<tr style=' border:1px solid black'>
<td>6</td>
<td></td>
<td></td>
</tr>
<tr style=' border:1px solid black'>
<td>7</td>
<td></td>
<td></td>
</tr> 
<tr style=' border:1px solid black'>
<td>8</td>
<td></td>
<td></td>
</tr>
<tr style=' border:1px solid black'>
<td>9</td>
<td></td>
<td></td>
</tr>
<tr style=' border:1px solid black'>
<td>10</td>
<td></td>
<td></td>
</tr>
<tr style=' border:1px solid black'>
<td>11</td>
<td></td>
<td></td>
</tr>
<tr style=' border:1px solid black'>
<td>12</td>
<td></td>
<td></td>
</tr>
<tr style=' border:1px solid black'>
<td>13</td>
<td></td>
<td></td>
</tr>
<tr style=' border:1px solid black'>
<td>14</td>
<td></td>
<td></td>
</tr>
<tr style=' border:1px solid black'>
<td>15</td>
<td></td>
<td></td>
</tr>
<tr style=' border:1px solid black'>
<td>16</td>
<td></td>
<td></td>
</tr>
</table>

> À l'aide de glisser/déplacer ou de copier/coller, placez les fichiers dans un dossier qui vous semble convenir.

### 1.4 - Envoi du travail demandé

> Quand l'opération est terminée, sélectionnez le dossier *Alice* et compressez le (clic droit puis *ajouter à l'archive*) sous le nom de dossier *monprenom_monnom_arbre.zip*.

> Envoyez le fichier d'achive ainsi créé à votre professeur via l'ENT SN5965.. 

## 2 : Activité en lignes de commande sous linux

### 2.1: Mise en route

> Ouvrez *Virtual Box* et lancez *Ubuntu*.

> Vous arrivez sur le bureau, choissez "lancer une fenêtre de commande" avec l'icône: ![ubuntu](commande_ubuntu.png)

Découvrons maintenant quelques commandes élémentaires:

- *tree*: affiche l'arborescence à partir du dossier courant.<br>
  
  > Exécutez la commande *tree* et observez. Les dossiers (ou répertoires) sont en bleu et les fichiers en blanc.
  > 
  > Si la commande n'est pas reconnue, dans ce cas, il faut procéder à son installation en tapant: *sudo install tree*.

- *ls* : liste le contenu du dossier courant: on voit alors apparaitre les  noms des fichiers contenus dans le dossier  courant et les sous-dossiers.

> Exécutez cette commande, vous devez voir apparaitre entre autres un dossier *documents*.

- *cd* : permet de changer de dossier  à partir du dossier courant.

> Exécutez la commande: *cd Documents*, vous arrivez à un affichage du type : ~/Documents&#36;<br>

Ce dossier est vide, nous allons créer un sous-dossier

- _mkdir_ : crée un dossier (make directory) .

> Tapez la commande *mkdir travail*.

> Vérifiez la création du dossier avec *ls*. Le dossier *travail* soit apparaitre.

> Placez-vous dans ce dossier à l'aide la commande *cd travail*. Le chemin indiqué est alors *~/Documents/travail&#36;* <br>

Revenons maintenant au niveau supérieur, donc dans le dossier *Documents*. 

> Pour cela tapez *cd ..*, en veillant bien à insérer un **espace** entre le *d* et le premier point.

> Créez un deuxième dossier *essai* et listez le contenu de *Documents* pour vérification.

> Placez-vous dans le dossier *essai*.

- *touch*: crée un fichier, par exemple *touch fic1* créer un fichier nommé *fic1*.

> Créez le fichier *fic1* et un fichier *fic2*.

> Vérifier le contenu du dossier *essai*.
> Exécutez à nouveau la commande *tree* et observez les modifications.
> 
> - *rm* : supprime (remove) un fichier.

> Supprimez le fichier fic2 par la commande *rm fic2*.

> Vérifiez en lisant le contenu du dossier.

Plaçons maintenant à nouveau dans la racine *Documents*.

- *rmdir*: supprime un dossier (remove directory).

> Demandez la suppresion du dossier *essai*.

Un message nous indique que le dossier n'est pas vide. Nous pourrions supprimer le fichier restant *fic1* mais nous allons plutôt le déplacer vers le dossier *travail*.

- *mv*: déplace ou renomme un fichier.

> Déplacez le fichier *fic1* vers *travail* en saisissant: *mv fic1 ../travail*

> Vérifiez que le dossier courant (*essai*) est maintenant vide et supprimez le.

> Placez-vous dans *travail* et vérifiez la présence de *fic1*.

Nous allons maitenant procéder à un renommage de fichier.

> Créez un fichier *fic3* dans la racine 'Documents*.

> Déplacez ce fichier dans *travail*.

> Renommez ce fichier en *fic2* en saisissant *mv fic3 fic2*.

Copions un fichier.

- *cp*: copie un fichier.

> Dans le dossier *travail*, saisissez *cp fic1 fic1bis*.

> Vérifiez la présence de trois fichiers : *fic1, fic2, fic1bis*.

> Renommez *fic1bis* en *fic3*.

> Exécutez la commande *tree*.

Il est peut-être temps de faire un peu de ménage...

- *clear*: vide le contenu de la console

### 2.2: Exercice

Créer un sous-dossier *personnel* au dossier *Documents*.  
Dans ce dossier, créer deux fichiers *fic4* et *fic5*.  
Déplacer *fic4* vers *travail* et *fic3* vers *personnel*Copier *fic4* en *fic4bis* puis supprimer *fic4*.  
Renommer *fic4bis* en *fic5*.

## 3: la commande *cat*

### 3.1 Activité préalable

> À partir d'*ubuntu*, lancez *Mozilla firefox* et rendez vous sur: <font color="blue"><center>![SavoirsNumeriques5962.fr](http://blaise-pascal.savoirsnumeriques5962.fr)</center></font>

> Téléchargez le fichier compressé ex_cat.zip (choisissez *save file*)

> Fermez le navigateur et revenez dans l'interface de commande linux, placez vous dans le dossier *Téléchargements*.

> Vérifiez la présence du fichier compressé *ex_cat.zip* et excécutez la commande *unzip ex_cat.zip*.

> Listez le contenu du dossier *Téléchargements* et vérifiez la présence d'un dossier *ex_cat*.

> Supprimez le fichier compressé.

> Déplacez le dossier *ex_cat* dans la racine.

### 3.2 Découverte

La commande *cat* permet à la fois d'afficher le contenu d'un fichier et de le modifier.

> En vous plaçant dans le dossier *ex_cat*, affichez le contenu de *essai1.txt* en saisissant *cat essai1.txt*  .

> Tapez maintenant *cat >> essai1.txt* <<fin

Il suffit maintenant de taper le contenu du texte que l'on veut ajouter, en terminant par le mot placé derrière "doubles chevrons <<" qui marquera l'arrêt de la modification du fichier.<br> 
Le double chevron après la commande *cat* permet d'ajouter du contenu à la suite du fichier.
Par exemple:

> Saisissez:  

<em>Hello World !  
ça marche  
fin<br></em>

> Vérifiez que le contenu du fichier est bien modifié.

De même, l'utilisation d'un simple chevron *>* va remplacer le contenu du fichier par le texte saisi.

> Saisissez *cat > essai1.txt <<fin*

Par exemple, remplaçons le contenu par:  
*On a tout changé!*

> Vérifiez la modification du contenu.

### 3.3 Création d'un fichier et concaténation

La commande *cat* peut créer un fichier.

> Saisissez *cat > essai2.txt <<fin*

on peut ajouter un contenu, par exemple:
*Voici un autre contenu*

> Vérifiez la création du fichier avec *ls* puis éditez le avec *cat*.

Nous allons maintenant concaténer (fusionner) les fichiers *essai1.txt* et *essai2.txt*.

> Saisissez *cat essai1.txt essai2.txt > essai3.txt*  .

Nous venons de créer un nouveau fichier *essai3.txt* .

> Vérifiez la création, éditez le contenu de *essai3.txt* .

> Supprimez *essai2.txt* et *essai3.txt* .

### 3.3: Exercice

> Créez un dossier *exercice* dans *Documents*.
> Créez un fichier *ex1.txt* contenant le texte:

*contenu numéro 1*

> De même créez un fichier *ex2.txt* contenant le texte:

*contenu numéro 2*

> Concaténez les deux fichiers dans le fichiers *ex3.txt*.

> Au fichier *ex3.txt* ajoutez le texte:

*contenu numéroi 3*

> Remplacez le contenu de *ex1.txt* par:

*contenu 1 a été changé*.

> Replacez vous au nivau supérieur dans l'arborescence.

Nous allons maintenant compresser le dossier *exercice* en le fichier *exercice.zip*

> Saisissez *zip exercice.zip exercice*

> Après avoir vérifié la présence du fichier *exercice.zip*, envoyez le au professeur via le navigateur et *SN5962*.
