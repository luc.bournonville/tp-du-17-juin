# Bloc 3 – systèmes – rédaction d'une fiche de cours / d'activité 

Vous allez rédiger une fiche d'activité correspondant à une séance
avec vos élèves sur un sujet relatif au bloc 3. 

Cette séance potera sur un point que vous aurez choisi parmi les
éléments abordés sur le système d'exploitation et l'interpréteur de
commandes (GitLab et VirtualBox sont donc exclus).

Cette fiche d'activité sera à réaliser en **binôme** pour la 1re
séance du bloc 3 de la semaine du 17 juin (ie a priori le 19 juin).

Pour cela, vous produirez un document au format Markdown que vous
partgaerez via un projet GitLab

* consultez éventuellement le [mini guide Markdown disponible sur le portail](../markdown0/readme.md)).

Vous pouvez inclure dans cette fiche les éléments listés ci-dessous. 

# Éléments de la fiche

## Sujet abordé
Donner brièvement le sujet abordé dans le cours.

## Objectifs
Quels sont les éléménts du programme de NIS visés, à la fois en terme
de contenu et de compétences. 

## Pré-requis
Quels sont les éventuels points qui doivent être connus des élèves
pour ce cours ?

## Préparation
Quels sont les éventuels besoins techniques ou en terme de
configuration nécessaires à la partie pratique ?

## Éléments de cours
Donner ici les éléments qui seront abordés dans le cours (on ne vous
demande pas d'écrire une leçon complète).

## Séance pratique
Décrivez ici les manipulations qui devront être réalisées par les
élèves pour illustrer les concepts abordés.

## QCM E3C
Quelles questions peuvent être proposées pour le QCM d'épreuves
communes de contrôle continu (E3C) en classe de première ? 
